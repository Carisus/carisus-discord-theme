#A Discord theme.

##Requirements

* For Windows only (Might work on Mac, I won't test it.)
* [Discord](https://discordapp.com/)
* [BetterDiscord](https://github.com/Jiiks/BetterDiscordApp/releases)
* [Carisus theme](https://bitbucket.org/Carisus/carisus-discord-theme/downloads/Carisus.theme.css)

##Installation

After downloading and installing the above

Navigate to C:\Users\UserName\AppData\Roaming\BetterDiscord\themes and place Carisus.theme.css there.

Open the Discordapp and click on the User Settings button.

![3d8282e7872eeef5452d1ec0568771c3.png](https://bitbucket.org/repo/8reXpb/images/804798444-3d8282e7872eeef5452d1ec0568771c3.png)

Then click on BetterDiscord, then Themes and select "Carisus Personal Theme". Click done.

You may need to restart Discord to get the theme to show up. If you are updating, press Ctl+R to refresh, no need to restart.

##Main Landing page
![Main Landing](http://imgur.com/i1GwyK3.png)

##User Settings
![User Settings](http://i.imgur.com/95h80B9.png)

##Member List Slider
![List Slider](http://imgur.com/HGmf7Cg.png)

##Member Details Popout
![Member Popout](http://imgur.com/gedJ98X.png)
